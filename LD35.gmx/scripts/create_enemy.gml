randomize( );
var rand_value = floor( random( 20 ) );
var x_pos = random(room_width);
var object;

if(x_pos < 40)
{
    x_pos += 40;
}
else if( x_pos > room_width - 40 )
{
    x_pos -= 40;
}

if( 0 <= rand_value and 5 >= rand_value )
{
    object = Square_Obj;
}
else if( 6 <= rand_value and 11 >= rand_value )
{
    object = Circle_Obj;
}
else if( 12 <= rand_value and 17 >= rand_value )
{
    object = Triangle_Obj;
}
else if( 18 == rand_value )
{
    object = Health_Obj;
}
else
{
    exit;
}

instance_create( x_pos, -9, object );
