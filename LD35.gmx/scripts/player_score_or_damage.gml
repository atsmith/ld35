var player = argument0;
var enemy = argument1;
var player_sprite = argument2;
var enemy_sprite = argument3;

if( Square_Spr == enemy_sprite )
{
    // easier comparison later
    enemy_sprite = Player_Square_Spr;
}
else if( Circle_Spr == enemy_sprite )
{
    enemy_sprite = Player_Circle_Spr;
}
else if( Triangle_Spr == enemy_sprite )
{
    enemy_sprite = Player_Triangle_Spr;
}

if( player_sprite == enemy_sprite )
{
    score += 1;
    audio_stop_sound(score_point);
    audio_play_sound( score_point, 10, false );
}
else if( health_pack_spr == enemy_sprite )
{
    if( 100 != health )
    {
        var health_val = 5;
        if( health + health_val > 100 )
        {
            health_val = 100 - health;
        }
        health += health_val;
        audio_stop_sound( health_gain );
        audio_play_sound( health_gain, 10, false );
    }
    else
    {
        score += 1;
        audio_stop_sound( score_point );
        audio_play_sound( score_point, 10, false );
    }
}
else
{
    with(player)
    {
        health -= 10;
        audio_stop_sound(hurt);
        audio_play_sound( hurt, 10, false );
        if(health <= 0)
        {
            audio_stop_sound(hurt);
            audio_play_sound( death_sound, 10, false );
            instance_destroy();
        }
    }
}

with(enemy)
{
    instance_destroy( );
}

